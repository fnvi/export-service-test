let router = require('express').Router();


let columns = [
    {
        header:'name'
    },
    {
        header:'done'
    },
    {
        header:'total'
    },
    {
        header:'percentage',
        format:'0.00%'
    }
];

let data = new Array(20).fill({}).map((row,i) => ({name:`Item ${i+1}`, done:i%10, total:10, percentage:(i%10)/10}))

router.get('/',(req,res)=>{
    res.json({
        columns:columns,
        data:data
    })
})

module.exports = router;