let router = require('express').Router();
let puppeteer = require('puppeteer');

router.get('/',(req,res)=>{
    printPDF(req.query.url)
    .then(
        pdf => {
            res.type('application/pdf');
            res.send(pdf);
        }
    )
})

async function printPDF(url) {
    const browser = await puppeteer.launch()
    const page = await browser.newPage();
    // page.setContent('<div>hello</div>');
    await page.goto(url, {waitUntil: 'networkidle0'});
    const pdf = await page.pdf({ format: 'A4' });
   
    await browser.close();
    return pdf
  }

module.exports = router