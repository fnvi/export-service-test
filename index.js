let app = require('express')();


app.use('/pdf',require('./routes/pdf'));
app.use('/xlsx',require('./routes/xlsx'));
app.use('/data',require('./routes/data'));

app.listen(3000)