let router = require('express').Router();
let Excel = require('exceljs');
let { unlink } = require('fs');

let columns = [
    {
        header:'name',
        key:'name',
        width:22,
        style:{
            alignment:{horizontal:'center'},
        }
    },
    {
        header:'done',
        key:'done'
    },
    {
        header:'total',
        key:'total',
        width:10,
    },
    {
        header:'percentage',
        key:'percentage',
        width:11,
        // outlineLevel:1,
        style:{
            numFmt:'0.0%',
        }
    }
];

let data = new Array(100).fill({}).map((row,i) => ({name:`Item ${i+1}`, done:i%10, total:10, percentage:(i%10)/10}))

let sheets = [{name:'test',data,columns}]

router.get('/',(req,res)=>{
    makeWorkbook(sheets).then(
        filename => {
            res.download(filename,(err)=>unlink(filename,(err)=>!err||console.error(err)));
        },
        error => res.status(500).json({message:'error',error})
    )
})


function makeWorkbook(sheets=[]){
    let filename = `${Date.now()}.xlsx`;
    let wb = new Excel.Workbook();
    let logo = wb.addImage({
        filename:'download.png',
        extension:'png'
    });
    sheets.forEach(
        ({name, data, columns}) =>{
            let sheet = wb.addWorksheet(name);
            sheet.addImage(logo,'A1:A1');
            sheet.views = [{state:'frozen',xSplit:1, ySplit:2}];
            sheet.autoFilter = {
                from:{
                    row:2, column:1
                },
                to:{row:2,column:columns.length}
            }
            sheet.columns = columns;
            sheet.addRows(data);
            sheet.spliceRows(1,0,new Array(2));
            sheet.getRow(1).height =45;
            sheet.pageSetup.printTitlesRow='1:2'
            // sheet.spliceColumns(1,0,[]);
            // data.forEach(row => sheet.addRow(row).commit())
            // sheet.commit();
        }
    )
    // return wb.xlsx.write()
    return wb.xlsx.writeFile(filename).then(() => filename)
}

function makeSheet({sheetName='', data=[], columns=[]}={}){
    let sheet = XLSX.utils.json_to_sheet(data,getHeaders(columns))

}


module.exports = router;