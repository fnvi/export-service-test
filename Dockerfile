FROM fnvi/puppeteer:latest

USER root

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

RUN chown -R pptruser:pptruser ./node_modules

USER pptruser

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]

