# Example export service using puppeteer #

To run this container you will need to add SYS_ADMIN capability

```bash
docker run -d --name export-service -p 3000:3000 --cap-add=SYS_ADMIN fnvi/export-service:latest
```

alternatively you can set this in a compose file

```yml
version: "3"
services:
  export-service:
    image: fnvi/export-servce:latest
    cap_add:
      - SYS_ADMIN
    ports:
      - 3000:3000
```